# KiCad Library Symbols and Footprints

**Battery_Holders.pretty**

Keystone 18650 Battery Holder 1042_ 
[Datasheet](http://www.farnell.com/datasheets/2067690.pdf)
[Mouser](https://www.mouser.co.uk/ProductDetail/Keystone-Electronics/1042P?qs=sGAEpiMZZMtqO%252bWUGLBzeAoSIwG%2fyfJm2NzM8DAiwxE%3d)

